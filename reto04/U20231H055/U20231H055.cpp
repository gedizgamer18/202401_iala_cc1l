#include <iostream>
#include <conio.h>

using namespace std;

int* armarMatriz(int n, int m) {
	
	int* matriz = new int[n * m];
	
	for (int i = 0; i < n * m; ++i) {

		matriz[i] = 0;
	}
	return matriz;
}

void impMatriz(int* matriz, int n, int m) {

	for (int i = 0; i < n; ++i) {

		for (int j = 0; j < m; ++j) {

			cout << matriz[i * m + j] << " ";
		}
		cout << endl;
	}
}


int main() {

	int n, m;

	cout << "INGRESAR NUMERO DE FILAS (n): ";
	cin >> n;

	cout << "INGRESAR NUMERO DE COLUMNAS (m): ";
	cin >> m;


	int* matriz = armarMatriz(n, m);

	cout << "\nMATRIZ DE " << n << " x " << m << endl;

	impMatriz(matriz, n, m);

	delete[] matriz;

	_getch();
	system("pause");
	return 0;

}