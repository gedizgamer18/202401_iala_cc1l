#include <iostream>
using namespace std;
int** crear_matrizd(int n, int m);
void imprimir_matriz(int **matriz, int n, int m);
int main() {
	srand(time(NULL));

	int n, m;
	cout << "Ingrese el numero de filas: "; cin >> n;
	cout << "Ingrese el numero de columnas: "; cin >> m;

	int** arreglo = crear_matrizd(n, m);
	imprimir_matriz(arreglo, n, m);
	
	for (int i = 0; i < n; i++) {
		delete[] arreglo[i];
	}
	delete[] arreglo;
	
	system("pause");
	return 0;
}

int** crear_matrizd(int n, int m) {
	
	int** matriz = new int* [n];
	for (int i = 0; i < n; i++) {
		matriz[i] = new int[m];
	}
	
	return matriz;
}

void imprimir_matriz( int n, int m) {
	int** matriz = crear_matriz(n, m);
	cout << "\nArreglo " << n << "x" << m << ": " << endl;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			matriz[i][j] = 0;
			cout << matriz[i][j];
			if (!(i == n - 1 && j == m - 1)) {
				cout << ", ";
			}
		}
		cout << "\n";
	}
}