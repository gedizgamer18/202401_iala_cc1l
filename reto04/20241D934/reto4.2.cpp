#include <iostream>
using namespace std;

int** CrearMatrizDinamica(int n, int m);
void ImprimirMatrizDinamica(int n, int m, int** matrizD);

int main() {

	int n, m;

	cout << "Ingrese n (filas): "; cin >> n;
	cout << "Ingrese m (columnas): "; cin >> m;

	int** matriz = CrearMatrizDinamica(n, m);
	ImprimirMatrizDinamica(n, m, matriz);

	for (int i = 0; i < n; ++i) {
		delete[] matriz[i];
	}

	delete[] matriz;

	system("pause");
	return 707;
}
int** CrearMatrizDinamica(int n, int m) {

	int** matrizD = new int* [n];
	for (int i = 0; i < n; i++) {
		matrizD[i] = new int[m];
	}
	return matrizD;
}

void ImprimirMatrizDinamica(int n, int m, int** matrizD) {

	cout << endl << "La matriz creada " << n << "x" << m << ":" << endl;
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			matrizD[i][j] = 0;

			if (j == 0) {
				cout << "|";
			}
			if (j == m - 1) {
				cout << matrizD[i][j] << "|";
			}
			else if (!(i == n - 1 && j == m - 1)) {
				cout << matrizD[i][j] << ",";
			}
		}
		cout << endl;
	}
}